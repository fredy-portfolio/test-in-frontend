# Pasos a seguir para correr el proyecto de React.

## Scripts disponibles

Abriendo una terminal en la carpeta base (test-in-frontend) puedes correr los siguientes comandos.

### `npm install`

Instala todas las dependencias que existan en el package.json. Siempre se corre antes del npm start.

### `npm start`

Abre la aplicación en modo de desarrollador en el puerto que tengas generado en tu .env

### `npm test`

Activa la función de test utilizando Jest.

### `npm run build`

Compila la versión actual de tu vista para luego poder subirla a un servidor.

### `npm run lint`

Valida si el código generado está con la estructura solicitada.

### `npm run lint:fix`

Arregla los problemas de Lint.

## Comandos para GIT.

### `git checkout RAMA`

Sirve para cambiar de rama.

### `git checkout -b RAMANUEVA RAMABASE`

Sirve para crear una nueva rama, donde a la izquierda va el nombre de la nueva rama y a la derecha el nombre de la rama en la cual te basas. Usualmente debería ser master. El nombre de la rama debe estar ligada a una tarea de jira, es decir por ejemplo TIT-1

### `git stash`

Es para descartar los cambios realizados y guardarlos en una rama temporal.

### `git stash drop y git stash apply`

Git stash drop elimina por completo los cambios que tienes almacenados en la rama temporal de git stash.
Git stash apply aplica los cambios guardados a la rama actual para que puedas seguir trabajando en caso de haberte equivocado de rama.

### `git add .`

Agrega todos los cambios realizados hasta la fecha y los junta para el commit.

### `git commit -m "NOMBRE DEL COMMIT"`

Aquí generas el commit con los cambios que colocaste con "git add ."

### `git push`

Simplemente sube a la rama en la que estás trabajando los cambios realizados con git commit.
